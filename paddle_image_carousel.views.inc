<?php

/**
 * Implements hook_views_data().
 */
function paddle_image_carousel_views_data() {
  $data['node']['title_link_internal_path'] = array(
    'group' => t('Paddle Image Carousel'),
    'real field' => 'nid',
    'title' => t('Title linked to the internal path field.'),
    'help' => t('Title linked to the value of the internal path field.'),
    'field' => array(
      'handler' => 'paddle_image_carousel_handler_title',
    ),
  );

  $data['node']['image_link_internal_path'] = array(
    'group' => t('Paddle Image Carousel'),
    'real field' => 'nid',
    'title' => t('Image linked to the internal path field.'),
    'help' => t('Image linked to the value of the internal path field.'),
    'field' => array(
      'handler' => 'paddle_image_carousel_handler_image',
    ),
  );

  return $data;
}