<?php
/**
 * @file
 * paddle_image_carousel.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function paddle_image_carousel_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'paddle_image_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Paddle Image Carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Paddle Image Carousel';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'paddle_image_carousel';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_carousel_date' => 'field_carousel_date',
    'field_carousel_location' => 'field_carousel_location',
  );
  $handler->display->display_options['row_options']['separator'] = ',';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Internal Path */
  $handler->display->display_options['fields']['field_internal_path']['id'] = 'field_internal_path';
  $handler->display->display_options['fields']['field_internal_path']['table'] = 'field_data_field_internal_path';
  $handler->display->display_options['fields']['field_internal_path']['field'] = 'field_internal_path';
  $handler->display->display_options['fields']['field_internal_path']['label'] = '';
  $handler->display->display_options['fields']['field_internal_path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_internal_path']['element_label_colon'] = FALSE;
  /* Field: Paddle Image Carousel: Image linked to the internal path field. */
  $handler->display->display_options['fields']['image_link_internal_path']['id'] = 'image_link_internal_path';
  $handler->display->display_options['fields']['image_link_internal_path']['table'] = 'node';
  $handler->display->display_options['fields']['image_link_internal_path']['field'] = 'image_link_internal_path';
  $handler->display->display_options['fields']['image_link_internal_path']['label'] = '';
  $handler->display->display_options['fields']['image_link_internal_path']['element_label_colon'] = FALSE;
  /* Field: Content: Slogan */
  $handler->display->display_options['fields']['field_carousel_slogan']['id'] = 'field_carousel_slogan';
  $handler->display->display_options['fields']['field_carousel_slogan']['table'] = 'field_data_field_carousel_slogan';
  $handler->display->display_options['fields']['field_carousel_slogan']['field'] = 'field_carousel_slogan';
  $handler->display->display_options['fields']['field_carousel_slogan']['relationship'] = 'carousel_item_target_id';
  $handler->display->display_options['fields']['field_carousel_slogan']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_slogan']['element_label_colon'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_carousel_date']['id'] = 'field_carousel_date';
  $handler->display->display_options['fields']['field_carousel_date']['table'] = 'field_data_field_carousel_date';
  $handler->display->display_options['fields']['field_carousel_date']['field'] = 'field_carousel_date';
  $handler->display->display_options['fields']['field_carousel_date']['relationship'] = 'carousel_item_target_id';
  $handler->display->display_options['fields']['field_carousel_date']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_date']['settings'] = array(
    'format_type' => 'short_day_month',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Paddle Image Carousel: Title linked to the internal path field. */
  $handler->display->display_options['fields']['title_link_internal_path']['id'] = 'title_link_internal_path';
  $handler->display->display_options['fields']['title_link_internal_path']['table'] = 'node';
  $handler->display->display_options['fields']['title_link_internal_path']['field'] = 'title_link_internal_path';
  $handler->display->display_options['fields']['title_link_internal_path']['label'] = '';
  $handler->display->display_options['fields']['title_link_internal_path']['element_label_colon'] = FALSE;
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_carousel_location']['id'] = 'field_carousel_location';
  $handler->display->display_options['fields']['field_carousel_location']['table'] = 'field_data_field_carousel_location';
  $handler->display->display_options['fields']['field_carousel_location']['field'] = 'field_carousel_location';
  $handler->display->display_options['fields']['field_carousel_location']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_location']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_carousel_image']['id'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['table'] = 'field_data_field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['field'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_image']['settings'] = array(
    'image_style' => 'paddle_image_carousel_full',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'paddle_image_carousel_items' => 'paddle_image_carousel_items',
  );

  /* Display: Paddle Image Carousel */
  $handler = $view->new_display('block', 'Paddle Image Carousel', 'paddle_image_carousel_block');
  $handler->display->display_options['block_description'] = 'Paddle Image Carousel';
  $translatables['paddle_image_carousel'] = array(
    t('Master'),
    t('Paddle Image Carousel'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Image'),
    t('All'),
  );
  $export['paddle_image_carousel'] = $view;

  return $export;
}
