<?php

/**
 * @file
 * Plugin to provide a custom content type pane,
 * Paddle Image Carousel, for the node edit/add panel.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Create a Carousel'),
  'description' => t('Form used to add items to a carousel.'),
  'category' => t('Paddle Landing Page'),
);

/**
 * Render callback.
 */
function paddle_image_carousel_carousel_items_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = $conf['carousel_title'];

  // Load view object.
  $view = views_get_view('paddle_image_carousel');
  // Define views display to use.
  $view->set_display('paddle_image_carousel_block');

  // Proccess arguments.
  // If we have more than one element, iterate through
  // and glue our arguments together.
  if (isset($conf['carousel_items']['item_2'])) {
    foreach ($conf['carousel_items'] as $item) {
      if (preg_match('/node\/(\d+)/', $item, $matches)) {
        $args[] = $matches[1];
      }
    }
    // String of args seperated with '+'.
    $args = implode('+', $args);
    // Trim trailing character.
    $args = rtrim($args, '+');
  }
  else {
    $args = isset($conf['carousel_items']['item_1']) ? $conf['carousel_items']['item_1'] : 0;
  }
  // Pass arguments (node id's) to view.
  $view->set_arguments(array($args));
  $view->execute();

  // Render view object.
  $block->content = $view->preview();

  return $block;
}

/**
 * Settings form builder callback.
 */
function paddle_image_carousel_carousel_items_content_type_edit_form($form, &$form_state) {
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;

  $form['carousel_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel title'),
    '#default_value' => isset($form_state['conf']['carousel_title']) ? $form_state['conf']['carousel_title'] : '',
  );

  $form['carousel_items'] = array(
    '#type' => 'fieldset',
    '#title' => t('Carousel items'),
    '#tree' => TRUE,
  );

  $form['carousel_items']['item_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel item #1'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['conf']['carousel_items']['item_1']) ? $form_state['conf']['carousel_items']['item_1'] : '',
    '#size' => 60,
    '#autocomplete_path' => 'carousel_item_autocomplete_callback',
  );

  $form['carousel_items']['item_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel item #2'),
    '#default_value' => isset($form_state['conf']['carousel_items']['item_2']) ? $form_state['conf']['carousel_items']['item_2'] : '',
    '#size' => 60,
    '#autocomplete_path' => 'carousel_item_autocomplete_callback',
  );

  $form['carousel_items']['item_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel item #3'),
    '#default_value' => isset($form_state['conf']['carousel_items']['item_3']) ? $form_state['conf']['carousel_items']['item_3'] : '',
    '#size' => 60,
    '#autocomplete_path' => 'carousel_item_autocomplete_callback',
  );

  $form['carousel_items']['item_4'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel item #4'),
    '#default_value' => isset($form_state['conf']['carousel_items']['item_4']) ? $form_state['conf']['carousel_items']['item_4'] : '',
    '#size' => 60,
    '#autocomplete_path' => 'carousel_item_autocomplete_callback',
  );

  $form['carousel_items']['item_5'] = array(
    '#type' => 'textfield',
    '#title' => t('Carousel item #5'),
    '#default_value' => isset($form_state['conf']['carousel_items']['item_5']) ? $form_state['conf']['carousel_items']['item_5'] : '',
    '#size' => 60,
    '#autocomplete_path' => 'carousel_item_autocomplete_callback',
  );

  return $form;
}

/**
 * Submit callback for the configuration form.
 */
function paddle_image_carousel_carousel_items_content_type_edit_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Displays the administrative title for a panel pane in the drag & drop UI.
 */
function paddle_image_carousel_carousel_items_content_type_admin_title($subtype, $conf, $context) {
  return t('Paddle Image Carousel');
}
