<?php
/**
 * @file
 * Views handler to display a title link on Paddle Image Carousel views.
 */

class paddle_image_carousel_handler_image extends views_handler_field_node_link {
  public $field_info;

  public function __construct() {
    // We do this to be able to work with flexslider.
    $this->field_info = array(
      'type' => 'image',
    );
  }

  /**
   * Renders the link.
   */
  public function render_link($node, $values) {
    // Ensure user has access to the administrative view of the node.
    $internal_path = field_get_items('node', $node, 'field_internal_path');
    if ($internal_path) {
      preg_match('/ \(node\/([0-9]+)\)$/', $internal_path[0]['value'], $matches);

      if (empty($matches[1]) || !drupal_valid_path('node/' . $matches[1])) {
        return;
      }
    }

    if (!empty($matches[1])) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = 'node/' . $matches[1];
    }
    else {
      $this->options['alter']['make_link'] = FALSE;
    }

    $image = field_get_items('node', $node, 'field_carousel_image');

    return theme('image_formatter', array('item' => $image[0], 'image_style' => 'paddle_image_carousel_full'));
  }
}
