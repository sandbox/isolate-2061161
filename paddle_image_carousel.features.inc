<?php
/**
 * @file
 * paddle_image_carousel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function paddle_image_carousel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array('version' => "1");
  }
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_views_api().
 */
function paddle_image_carousel_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function paddle_image_carousel_node_info() {
  $items = array(
    'paddle_image_carousel_items' => array(
      'name' => t('Carousel items'),
      'base' => 'node_content',
      'description' => t('Use this content type to create new carousel items.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
