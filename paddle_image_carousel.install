<?php

/**
 * @file
 * Install, uninstall, update and schema hooks for Paddle Image Carousel.
 */

/**
 * Implements hook_install().
 */
function paddle_image_carousel_install() {
  module_enable(array('flexslider_views'));
}

/**
 * Implements hook_enable().
 */
function paddle_image_carousel_enable() {
  module_enable(array('flexslider_views'));

  drupal_load('module', 'paddle_image_carousel');
  features_include_defaults(NULL, TRUE);
  features_revert();
  node_type_cache_reset();
  drupal_static_reset();

  $roles = user_roles();
  foreach ($roles as $rid => $name) {
    if ($name == 'Chief Editor' || $name == 'Editor') {
      user_role_grant_permissions($rid, array(
        'edit carousel_items content in landing pages',
        'create paddle_image_carousel_items content',
        'edit own paddle_image_carousel_items content',
        'edit any paddle_image_carousel_items content',
        'delete own paddle_image_carousel_items content',
        'delete any paddle_image_carousel_items content',
      ));
    }
  }
}

/**
 * Implements hook_disable().
 *
 * @todo This assumes that no other modules are dependant on flexslider.
 * Get a list of dependencies first and check that other modules are not
 * dependant on the modules we want to disable.
 */
function paddle_image_carousel_disable() {
  module_disable(array('flexslider_views', 'flexslider'), FALSE);
}
